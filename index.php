<?php

require 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('views');
$twig = new Twig_Environment($loader); 

$index = $twig->load('index.html.twig');

if(isset($_GET['dataPrevious'])){ // Appuie sur bouton prévious
    
    // Actualisation de la barre d'url

    $fetchUrl = $_GET['dataPrevious'];
    $url = preg_split("/[\/,]+/",$fetchUrl);
    $next = array_pop($url);
    $firstUrl = array_shift($url);
    $urlLength = count($url);
    $newPath = "";
    
    foreach($url as $i){
        $newPath = $newPath.'/'.$i;
    };

    $renderNav = $twig->render('nav.html.twig', array('chemin'=>$newPath));
   

  //   // Actualisation de la visualisation

   $message = icones($newPath);
   natcasesort($message);
   $nbElement = count($message);
   $taille = dirsize($newPath);
   $tailleMo = $taille / 1048576;
   $formatTailleMo = number_format($tailleMo, 2, ',','');

  $renderVisu = $twig->render('visu.html.twig', array('lien'=>implode($message), 'nombre'=>($nbElement." éléments,"), 'taille'=>("taille : ".$formatTailleMo." Mo")));

  // retouor FETCH

  $myArray = ['renderNav' => $renderNav,'renderVisu' => $renderVisu, 'newUrl' => $newPath];
  echo json_encode($myArray);

} elseif (isset($_GET['dataNext'])){ //Appuie sur bouton next

    $nextUrl = $_GET['dataNext'];

    $renderNav = $twig->render('nav.html.twig', array('chemin'=>$nextUrl));

     //   // Actualisation de la visualisation

   $message = icones($nextUrl);
   natcasesort($message);
   $nbElement = count($message);
   $taille = dirsize($nextUrl);
   $tailleMo = $taille / 1048576;
   $formatTailleMo = number_format($tailleMo, 2, ',','');

  $renderVisu = $twig->render('visu.html.twig', array('lien'=>implode($message), 'nombre'=>($nbElement." éléments,"), 'taille'=>("taille : ".$formatTailleMo." Mo")));

  // retouor FETCH

  $myArray = ['renderNav' => $renderNav,'renderVisu' => $renderVisu, 'newUrl' => $nextUrl];
  echo json_encode($myArray);

} elseif (isset($_GET['dataHome'])){ // Appuie sur bouton Home

    $homeUrl = $_GET['dataHome'];

    $renderNav = $twig->render('nav.html.twig', array('chemin'=>$homeUrl));
    
    // Actualisation de la visualisation

   $message = icones($homeUrl);
   natcasesort($message);
   $nbElement = count($message);
   $taille = dirsize($homeUrl);
   $tailleMo = $taille / 1048576;
   $formatTailleMo = number_format($tailleMo, 2, ',','');

  $renderVisu = $twig->render('visu.html.twig', array('lien'=>implode($message), 'nombre'=>($nbElement." éléments,"), 'taille'=>("taille : ".$formatTailleMo." Mo")));

  // retouor FETCH

  $myArray = ['renderNav' => $renderNav,'renderVisu' => $renderVisu, 'newUrl' => $homeUrl];
  echo json_encode($myArray);

}  elseif (isset($_GET['dataClique'])){

  $cliqueUrl = $_GET['dataClique'];

  $renderNav = $twig->render('nav.html.twig', array('chemin'=>$cliqueUrl));
  
  // Actualisation de la visualisation

 $message = icones($cliqueUrl);
 natcasesort($message);
 $nbElement = count($message);
 $taille = dirsize($cliqueUrl);
 $tailleMo = $taille / 1048576;
 $formatTailleMo = number_format($tailleMo, 2, ',','');

$renderVisu = $twig->render('visu.html.twig', array('lien'=>implode($message), 'nombre'=>($nbElement." éléments,"), 'taille'=>("taille : ".$formatTailleMo." Mo")));

// retouor FETCH

$myArray = ['renderNav' => $renderNav,'renderVisu' => $renderVisu, 'newUrl' => $cliqueUrl];
echo json_encode($myArray);

} else { //Chargement de la page par défault 
    
  $fileOne = realpath(getcwd());
  
  $message = icones($fileOne);
  natcasesort($message);
  $nbElement = count($message);
  $taille = dirsize($fileOne);
  $tailleMo = $taille / 1048576;
  $formatTailleMo = number_format($tailleMo, 2, ',','');

  echo $twig->render('templates.html.twig', array('chemin'=>realpath($fileOne),'lien'=>implode($message), 'nombre'=>($nbElement." éléments,"), 'taille'=>("taille : ".$formatTailleMo." Mo")));
}


function icones ($w){

  $ext1 = '.php';
  $ext2 = '.js';
  $ext3 = '.html';
  $ext4 = '.png';
  $ext5 = '.css';
  $ext6 = '.json';
  $ext7 = '.rst';
  $ext8 = '.git';
  $ext9 = '.md';
  $ext10 = '.twig';
  $ext11 = '.lock';
  $ext12 = '.scss';
  
  if ($dh = opendir($w)) {
  while (($file = readdir($dh)) !== false) {
  if(substr($file,-4) == $ext1){
  $message[] = '<div class="fichier"><img src="Icone/formPhp.png" height=100px width=80px alt="icone format php" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-3) == $ext2){
  $message[] = '<div class="fichier"><img src="Icone/formJs.png" height=100px width=80px alt="icone format js" class="fichier"></br><p>'.$file. '</p></div><br>';
  } else
  if(substr($file,-5) == $ext3){
  $message[] = '<div class="fichier"><img src="Icone/formHtml.png" height=100px width=80px alt="icone format html" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-4) == $ext4){
  $message[] = '<div class="fichier"><img src="Icone/formPng.png" height=100px width=80px alt="icone format png" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-4) == $ext5){
  $message[] = '<div class="fichier"><img src="Icone/formCss.png" height=100px width=80px alt="icone format css" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-5) == $ext6){
  $message[] = '<div class="fichier"><img src="Icone/formJson.png" height=100px width=70px alt="icone format json" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-4) == $ext7){
  $message[] = '<div class="fichier"><img src="Icone/formRst.png" height=100px width=80px alt="icone format rst" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-4) == $ext8){
  $message[] = '<div class="fichier"><img src="Icone/formGit.png" height=100px width=80px alt="icone format git" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-3) == $ext9){
  $message[] = '<div class="fichier"><img src="Icone/formRead.png" height=100px width=80px alt="icone format md" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-5) == $ext10){
  $message[] = '<div class="fichier"><img src="Icone/formTwig.png" height=100px width=70px alt="icone format twig" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-5) == $ext11){
  $message[] = '<div class="fichier"><img src="Icone/formLock.png" height=100px width=80px alt="icone format lock" class="fichier"></br><p>'. $file. '</p></div><br>';
  } else
  if(substr($file,-5) == $ext12){
    $message[] = '<div class="fichier"><img src="Icone/formScss.png" height=100px width=80px alt="icone format scss" class="fichier"></br><p>'. $file. '</p></div><br>';
    } else
  if(stristr($file, '.') === FALSE) {
  $message[] = ' <div class="dossier"><img src="Icone/folder.png" height=100px width=100px alt="icone dossiers"></br><p>'. $file. '</p></div><br>';;
  }  else 
  if($file != '.' && $file != '..'){
    $message[] = ' <div class="fichier"><img src="Icone/formIndef.png" height=60px width=40px alt="icone inconnue"></br><p>'. $file. '</p></div><br>';
   }
  
  }
  return $message;
  }
  }

  function dirsize($base) {
    $size = 0;
    if($dir = opendir($base)) {
      while($entry = readdir($dir)) {
        if(!in_array($entry, array(".",".."))) {
          if(is_dir($base."/".$entry)) {
            $size += dirsize($base."/".$entry);
          } else {
            $size += filesize($base."/".$entry);
          }
        }
      }
      closedir($dir);
      }
      return $size;
    }