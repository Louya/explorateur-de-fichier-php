const homeUrl = document.querySelector('#adresse').innerHTML;
let nextUrl = homeUrl;

// NAVIGATION DANS LES FICHIERS ----------------------------------------
// Clique sur dossier 
document.addEventListener('click', (e) => {
    e.preventDefault();
 
    if (e.target.parentElement.classList.contains('dossier')) {
        let cible = e.target.parentElement.children[2].textContent;
        console.log(cible);
        let cliqueUrl = document.querySelector('#adresse').innerHTML;
        const sep = "/";
        cliqueUrl = cliqueUrl.concat(sep).concat(cible);
        console.log(cliqueUrl);

        // Ajax
        fetch(`index.php?dataClique=${cliqueUrl}`)
            .then((resultClique) => {
                return resultClique.json()
            })
            .then(resultClique => {
                const newNav = document.querySelector('#url');
                const newVisu = document.querySelector('#blockVisu');
                console.log(newNav.innerHTML);
                console.log(resultClique.renderNav);
                newNav.innerHTML = resultClique.renderNav;
                newVisu.innerHTML = resultClique.renderVisu;
            })
    } 
});

// bouton home
const home = document.querySelector('#home');
home.addEventListener('click', function (e) {

    nextUrl = homeUrl;

    // Ajax
    fetch(`index.php?dataHome=${homeUrl}`)
        .then((resultHome) => {
            return resultHome.json()
        })
        .then(resultHome => {
            const newNav = document.querySelector('#url');
            const newVisu = document.querySelector('#blockVisu');
            newNav.innerHTML = resultHome.renderNav;
            newVisu.innerHTML = resultHome.renderVisu;
        })
});

// Bouton retour de la navigation

const previous = document.querySelector('#previous');
previous.addEventListener('click', function (e) {

    let actualUrl = document.querySelector('#adresse').innerHTML;

    // Ajax
    fetch(`index.php?dataPrevious=${actualUrl}`)
        .then((resultPre) => {
            return resultPre.json()
        })
        .then(resultPre => {
            const newNav = document.querySelector('#url');
            const newVisu = document.querySelector('#blockVisu');
            newNav.innerHTML = resultPre.renderNav;
            newVisu.innerHTML = resultPre.renderVisu;
        })
    nextUrl = actualUrl;
    return nextUrl;
})


// Bouton suivant de la navigation 

const next = document.querySelector('#next');
next.addEventListener('click', function (e) {

    // Ajax
    fetch(`index.php?dataNext=${nextUrl}`)
        .then((resultNext) => {
            return resultNext.json()
        })
        .then(resultNext => {
            const newNav = document.querySelector('#url');
            const newVisu = document.querySelector('#blockVisu');
            newNav.innerHTML = resultNext.renderNav;
            newVisu.innerHTML = resultNext.renderVisu;
        })
});